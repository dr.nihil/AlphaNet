package ru.my.microservice.interceptors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
public class LoggerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        try {
            log.info("[preHandle][" + new String(request.getInputStream().readAllBytes(), StandardCharsets.UTF_8) + "]" + "[" + request.getMethod()
                    + "]" + request.getRequestURI());
        } catch (IOException e) {
            log.info("[preHandle] Can't read body");
        }
        return true;
    }
}
