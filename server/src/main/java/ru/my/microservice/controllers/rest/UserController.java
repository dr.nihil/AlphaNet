package ru.my.microservice.controllers.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.my.microservice.Picture;
import ru.my.microservice.User;
import ru.my.microservice.dao.UserDAO;
import ru.my.microservice.exceptions.ObjectAlreadyExists;
import ru.my.microservice.exceptions.ObjectNotFoundException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {
    private final UserDAO userDAO;

    public UserController(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> createUser(@RequestBody User user) {
        log.info("Try to create user with nickname");
        try {
            return new ResponseEntity<>(userDAO.save(user), HttpStatus.OK);
        } catch (ObjectAlreadyExists e) {
            log.error("Can't create user with nickname");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public User getUser(@PathVariable long id) {
        try {
            return userDAO.getUserById(id);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id " + id + " not found", e);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<User> getUsers(@RequestParam(required = false) Integer max, @RequestParam(required = false) Integer offset) {
        return userDAO.getUsers(max, offset);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable long id, @RequestParam(required = false) Optional<Boolean> hard) {
        try {
            userDAO.delete(id, hard.orElse(false));
            return new ResponseEntity<>("User with id " + id + " is deleted", HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id " + id + " not found", e);
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    @ResponseBody
    public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable long id) {
        try {
            user.setId(id);
            return new ResponseEntity<>(userDAO.update(user), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id " + id + " not found", e);
        } catch (ObjectAlreadyExists e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/pic")
    public ResponseEntity<List<Picture>> addPictureToUser(@RequestBody Picture pic, @PathVariable long id) {
        try {
            return new ResponseEntity<>(userDAO.addPicture(id, pic), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id " + id + " not found", e);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/pic")
    public ResponseEntity<Picture> getActivePicture(@PathVariable long id) {
        try {
            return new ResponseEntity<>(userDAO.getActivePicture(id), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id " + id + " not found", e);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/subscriptions")
    @ResponseBody
    public List<User> createSubscriptionToUser(@PathVariable long id, @RequestParam long toUserId) {
        try {
            return userDAO.createSubscription(id, toUserId);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}/subscriptions")
    public ResponseEntity<List<User>> deleteSubscription(@PathVariable long id, @RequestParam long toUserId) {
        try {
            return new ResponseEntity<>(userDAO.deleteSubscription(id, toUserId), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/subscriptions")
    public ResponseEntity<List<User>> getSubscriptions(@PathVariable long id) {
        try {
            return new ResponseEntity<>(userDAO.getSubscriptions(id), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/subscriptors")
    public ResponseEntity<List<User>> getSubscriptors(@PathVariable long id) {
        try {
            return new ResponseEntity<>(userDAO.getSubscriptors(id), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }
}
