package ru.my.microservice.exceptions;

public class ObjectAlreadyExists extends Exception {
    public ObjectAlreadyExists(String message) {
        super(message);
    }
}
