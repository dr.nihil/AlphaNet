package ru.my.microservice.dao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.my.microservice.Contact;
import ru.my.microservice.Picture;
import ru.my.microservice.User;
import ru.my.microservice.exceptions.ObjectAlreadyExists;
import ru.my.microservice.exceptions.ObjectNotFoundException;
import ru.my.microservice.repositories.UserRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class UserDAOTest {

    private UserRepository userRepository;
    private CityDAO cityDAO;
    private SkillDAO skillDAO;
    private ContactDAO contactDAO;

    @Test
    void save() {
        userRepository = Mockito.mock(UserRepository.class);
        cityDAO = Mockito.mock(CityDAO.class);
        skillDAO = Mockito.mock(SkillDAO.class);
        contactDAO = Mockito.mock(ContactDAO.class);
        User user = new User("user_nickname");
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("email", "ma@ma"));
        contacts.add(new Contact("phone", "12341234"));
        user.setContacts(contacts);
        Mockito.when(userRepository.save(user)).thenReturn(user);
        Mockito.when(cityDAO.save(user.getCity())).thenReturn(user.getCity());
        Mockito.when(skillDAO.saveAll(user.getSkills())).thenReturn(user.getSkills());

        Mockito.when(contactDAO.exists(contacts)).thenReturn(List.of(contacts.get(1)));
        UserDAO userDAO1 = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        Assertions.assertThrows(ObjectAlreadyExists.class, () -> userDAO1.save(user));

        Mockito.when(contactDAO.exists(contacts)).thenReturn(new ArrayList<>());
        UserDAO userDAO2 = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        try {
            User actualUser = userDAO2.save(user);
            Assertions.assertEquals(user.getNickname(), actualUser.getNickname());
        } catch (ObjectAlreadyExists e) {
            Assertions.fail("We mustn't get exception in this case");
        }
    }

    @Test
    void delete(){
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);

        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);
        try {
            userDAO.delete(user.getId(), true);
            Mockito.verify(userRepository).delete(user);

            userDAO.delete(user.getId(), false);
            Mockito.verify(userRepository).markAsDeleted(user.getId());
        } catch (ObjectNotFoundException e) {
            Assertions.fail("We mustn't get exception in this case");
        }
    }

    @Test
    void getUserById() {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));

        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        try {
            User actualUser = userDAO.getUserById(user.getId());
            Assertions.assertEquals(user.getNickname(), actualUser.getNickname());
            Assertions.assertEquals(user.getId(), actualUser.getId());
        } catch (ObjectNotFoundException e) {
            Assertions.fail("We mustn't get exception in this case");
        }
        user.setDeleted(true);

        Assertions.assertThrows(ObjectNotFoundException.class, () -> userDAO.getUserById(user.getId()));
    }

    @Test
    void addPicture_initialListIsNull() {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        Picture picture = new Picture("picture1".getBytes(), true);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);
        Mockito.when(userRepository.save(user)).thenReturn(user);
        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        try {
            List<Picture> actualPictures = userDAO.addPicture(user.getId(), picture);
            Assertions.assertEquals(1, actualPictures.size());
            Assertions.assertTrue(actualPictures.get(0).isActual());
        } catch (ObjectNotFoundException e) {
            Assertions.fail("We mustn't get exception in this case");
        }
    }

    @Test
    void addPicture_initialListNotNull() {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        List<Picture> pictures = new ArrayList<>();
        pictures.add(new Picture("picture1".getBytes(), true));
        user.setPics(pictures);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);
        Mockito.when(userRepository.save(user)).thenReturn(user);
        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        try {
            List<Picture> actualPictures = userDAO.addPicture(user.getId(), new Picture("picture2".getBytes(), true));
            Assertions.assertEquals(2, actualPictures.size());
            Assertions.assertTrue(actualPictures.stream().filter(p -> Arrays.equals(p.getImage(), "picture2".getBytes())).findFirst().get().isActual());
        } catch (ObjectNotFoundException e) {
            Assertions.fail("We mustn't get exception in this case");
        }
    }

    @Test
    void getActivePicture() {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        List<Picture> pictures = new ArrayList<>();
        pictures.addAll(List.of(new Picture("picture1".getBytes(), false), new Picture("picture2".getBytes(), false)));
        user.setPics(pictures);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);
        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        Assertions.assertThrows(ObjectNotFoundException.class, () -> userDAO.getActivePicture(user.getId()));
        user.getPics().add(new Picture("picture3".getBytes(), true));

        try {
            Picture activePicture = userDAO.getActivePicture(user.getId());
            Assertions.assertTrue(activePicture.isActual());
            Assertions.assertTrue(Arrays.equals("picture3".getBytes(), activePicture.getImage()));
        } catch (ObjectNotFoundException e) {
            Assertions.fail("We mustn't get exception in this case");

        }
    }

    @Test
    void createSubscription_startUserNotExists() {
        userRepository = Mockito.mock(UserRepository.class);
        long startUserId = 1;
        long endUserId = 2;
        Mockito.when(userRepository.existsById(startUserId)).thenReturn(false);

        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        Assertions.assertThrows(ObjectNotFoundException.class, () -> userDAO.createSubscription(startUserId, endUserId));
    }

    @Test
    void createSubscription_endUserNotExists() {
        userRepository = Mockito.mock(UserRepository.class);
        long startUserId = 1;
        long endUserId = 2;
        Mockito.when(userRepository.existsById(startUserId)).thenReturn(true);
        Mockito.when(userRepository.existsById(endUserId)).thenReturn(false);

        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        Assertions.assertThrows(ObjectNotFoundException.class, () -> userDAO.createSubscription(startUserId, endUserId));
    }

    @Test
    void getSubscriptions_none() throws ObjectNotFoundException {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);

        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        List<User> subscriptions = userDAO.getSubscriptions(user.getId());
        Assertions.assertNotNull(subscriptions);
        Assertions.assertTrue(subscriptions.isEmpty());
    }

    @Test
    void getSubscriptions_withDeletedUsers() throws ObjectNotFoundException {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname_main");
        user.setId(99);
        List<User> subscriptions = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User u = new User("user_nickname" + i);
            u.setId(i);
            if ( i % 2 == 0) {
                u.setDeleted(true);
            }
            subscriptions.add(u);
        }
        user.setSubscriptions(subscriptions);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);

        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        List<User> actualSubscriptions = userDAO.getSubscriptions(user.getId());
        Assertions.assertEquals(2, actualSubscriptions.size());
        Assertions.assertFalse(actualSubscriptions.stream().anyMatch(User::isDeleted));
    }


    @Test
    void getSubscriptors_none() throws ObjectNotFoundException {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);

        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        List<User> subscriptors = userDAO.getSubscriptors(user.getId());
        Assertions.assertNotNull(subscriptors);
        Assertions.assertTrue(subscriptors.isEmpty());
    }

    @Test
    void getSubscriptors_withDeletedUsers() throws ObjectNotFoundException {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname_main");
        user.setId(99);
        List<User> subscriptors = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User u = new User("user_nickname" + i);
            u.setId(i);
            if ( i % 2 == 0) {
                u.setDeleted(true);
            }
            subscriptors.add(u);
        }
        user.setSubscriptors(subscriptors);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);

        UserDAO userDAO = new UserDAO(userRepository, cityDAO, skillDAO, contactDAO);

        List<User> actualSubscriptors = userDAO.getSubscriptors(user.getId());
        Assertions.assertEquals(2, actualSubscriptors.size());
        Assertions.assertFalse(actualSubscriptors.stream().anyMatch(User::isDeleted));
    }
}