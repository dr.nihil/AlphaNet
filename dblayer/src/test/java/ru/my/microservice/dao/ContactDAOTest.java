package ru.my.microservice.dao;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.my.microservice.Contact;
import ru.my.microservice.repositories.ContactRepository;

class ContactDAOTest {

    private ContactRepository contactRepository;

    @Test
    void exists() {
        contactRepository = Mockito.mock(ContactRepository.class);
        Mockito.when(contactRepository.existsByValue("ma1@email.com")).thenReturn(false);
        Mockito.when(contactRepository.existsByValue("ma2@email.com")).thenReturn(true);
        Mockito.when(contactRepository.existsByValue("ma3@email.com")).thenReturn(false);
        ContactDAO contactDAO = new ContactDAO(contactRepository);
        List<Contact> contacts = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            contacts.add(new Contact("email", String.format("ma%d@email.com", i)));
        }

        List<Contact> existedContacts = contactDAO.exists(contacts);
        Assertions.assertEquals(1, existedContacts.size());
        Assertions.assertEquals("ma2@email.com", existedContacts.get(0).getValue());
    }
}