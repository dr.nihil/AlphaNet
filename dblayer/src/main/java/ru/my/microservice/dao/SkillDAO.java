package ru.my.microservice.dao;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.my.microservice.Skill;
import ru.my.microservice.repositories.SkillRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SkillDAO {
    private final SkillRepository skillRepository;

    public SkillDAO(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    public Skill save(Skill skill){
        if (skill == null) {
            return null;
        }
        if (skill.getId() != 0) {
            return skillRepository.getReferenceById(skill.getId());
        } else {
            return Optional.ofNullable(skillRepository.findByName(skill.getName()))
                    .orElseGet(() -> skillRepository.save(skill));
        }
    }

    @Transactional
    public List<Skill> saveAll(List<Skill> skills) {
        if (skills == null || skills.isEmpty()) {
            return skills;
        }
        return skills.stream()
                .map(this::save)
                .collect(Collectors.toList());
    }
}
