package ru.my.microservice.dao;

import org.springframework.stereotype.Service;
import ru.my.microservice.Contact;
import ru.my.microservice.repositories.ContactRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContactDAO {
    private final ContactRepository contactRepository;

    public ContactDAO(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    public boolean exists(Contact contact){
        return contactRepository.existsByValue(contact.getValue());
    }

    public List<Contact> exists(List<Contact> contacts){
        if (contacts == null) {
            return contacts;
        }
        return contacts.stream()
                .filter(this::exists)
                .collect(Collectors.toList());
    }
}
