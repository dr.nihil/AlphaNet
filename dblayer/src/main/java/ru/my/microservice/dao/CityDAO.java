package ru.my.microservice.dao;

import org.springframework.stereotype.Service;
import ru.my.microservice.City;
import ru.my.microservice.repositories.CityRepository;

import java.util.Optional;

@Service
public class CityDAO {
    private final CityRepository cityRepository;

    public CityDAO(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public City save(City city){
        if (city == null) {
            return null;
        }
        if (city.getId() != 0) {
            return cityRepository.getReferenceById(city.getId());
        } else {
            return Optional.ofNullable(cityRepository.findByName(city.getName()))
                    .orElseGet(() -> cityRepository.save(city));
        }
    }
}
