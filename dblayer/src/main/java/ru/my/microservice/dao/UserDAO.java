package ru.my.microservice.dao;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.my.microservice.Contact;
import ru.my.microservice.Picture;
import ru.my.microservice.User;
import ru.my.microservice.exceptions.ObjectAlreadyExists;
import ru.my.microservice.exceptions.ObjectNotFoundException;
import ru.my.microservice.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserDAO {

    private final UserRepository userRepository;
    private final CityDAO cityDAO;
    private final SkillDAO skillDAO;
    private final ContactDAO contactDAO;


    private final static Integer DEFAULT_MAX = 10;

    public UserDAO(UserRepository userRepository, CityDAO cityDAO, SkillDAO skillDAO, ContactDAO contactDAO) {
        this.userRepository = userRepository;
        this.cityDAO = cityDAO;
        this.skillDAO = skillDAO;
        this.contactDAO = contactDAO;
    }

    @Transactional
    public User save(User user) throws ObjectAlreadyExists {
        List<Contact> contacts = contactDAO.exists(user.getContacts());
        if (contacts != null && !contacts.isEmpty()){
            throw new ObjectAlreadyExists("Contacts with values " + contacts.stream().map(c -> c.getValue()).collect(Collectors.toList()) + " already exist");
        }
        user.setSkills(skillDAO.saveAll(user.getSkills()));
        user.setCity(cityDAO.save(user.getCity()));
        return userRepository.save(user);
    }

    public User getUserById(long userId) throws ObjectNotFoundException {
        User user = userRepository.findById(userId).orElseThrow(() -> new ObjectNotFoundException("User with id " + userId + " not found"));
        checkUserDeleted(user);
        return user;
    }

    public List<User> getUsers(Integer max, Integer offset) {
        return userRepository.getUsers(Optional.ofNullable(max).orElse(DEFAULT_MAX), Optional.ofNullable(offset).orElse(0));
    }

    @Transactional
    public boolean delete(long userId, boolean hard) throws ObjectNotFoundException {
        User user = userRepository.getReferenceById(userId);
        checkUserDeleted(user);
        if (hard) {
            userRepository.delete(user);
        } else {
            userRepository.markAsDeleted(userId);
        }
        return true;
    }

    public User update(User user) throws ObjectNotFoundException, ObjectAlreadyExists {
        User oldUser = userRepository.getReferenceById(user.getId());
        checkUserDeleted(oldUser);
        return save(user);
    }

    @Transactional
    public List<Picture> addPicture(long userId, Picture pic) throws ObjectNotFoundException {
        User user = userRepository.getReferenceById(userId);
        checkUserDeleted(user);
        user.addPicture(pic);
        userRepository.save(user);
        return userRepository.getReferenceById(userId).getPics();
    }

    @Transactional
    public Picture getActivePicture(long userId) throws ObjectNotFoundException {
        User user = userRepository.getReferenceById(userId);
        checkUserDeleted(user);
        List<Picture> pictures = user.getPics();
        if (pictures == null){
            throw new ObjectNotFoundException("User doesn't have pictures");
        }
        return pictures.stream()
                .filter(p -> p.isActual())
                .findFirst()
                .orElseThrow(() -> new ObjectNotFoundException("Active picture isn't found"));
    }

    @Transactional
    public List<User> createSubscription(long startUserId, long endUserId) throws ObjectNotFoundException {
        checkUserExistsById(startUserId);
        checkUserExistsById(endUserId);
        userRepository.createSubscription(startUserId, endUserId);
        return getSubscriptions(startUserId);
    }

    @Transactional
    public List<User> deleteSubscription(long startUserId, long endUserId) throws ObjectNotFoundException {
        checkUserExistsById(startUserId);
        checkUserExistsById(endUserId);
        userRepository.deleteSubscription(startUserId, endUserId);
        return getSubscriptions(startUserId);
    }

    @Transactional
    public List<User> getSubscriptions(long userId) throws ObjectNotFoundException {
        User user = userRepository.getReferenceById(userId);
        checkUserDeleted(user);
        return user.getSubscriptions();
    }

    @Transactional
    public List<User> getSubscriptors(long userId) throws ObjectNotFoundException {
        User user = userRepository.getReferenceById(userId);
        checkUserDeleted(user);
        return user.getSubscriptors();
    }

    private void checkUserDeleted(User user) throws ObjectNotFoundException {
        if (user.isDeleted()){
            throw new ObjectNotFoundException("User with id " + user.getId() + " doesn't exist");
        }
    }

    private void checkUserExistsById(long userId) throws ObjectNotFoundException{
        if (!userRepository.existsById(userId)){
            throw new ObjectNotFoundException("User with id " + userId + " not found");
        }
    }
}
