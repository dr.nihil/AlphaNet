package ru.my.microservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.my.microservice.Contact;

public interface ContactRepository extends JpaRepository<Contact, Long> {
    boolean existsByValue(String value);
}
