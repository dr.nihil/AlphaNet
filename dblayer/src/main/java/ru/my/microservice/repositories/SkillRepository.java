package ru.my.microservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.my.microservice.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long> {
    public Skill findByName(String name);
}
