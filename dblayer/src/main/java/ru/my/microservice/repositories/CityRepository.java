package ru.my.microservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.my.microservice.City;

public interface CityRepository extends JpaRepository<City, Long> {
    public City findByName(String name);
}
