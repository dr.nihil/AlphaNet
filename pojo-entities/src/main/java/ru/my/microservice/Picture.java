package ru.my.microservice;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "pic")
@Data
public class Picture {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private long id;
    @Column(name = "image")
    private byte[] image;
    @Column(name = "is_actual")
    private boolean isActual;

    public Picture(byte[] image, boolean isActual) {
        this.image = image;
        this.isActual = isActual;
    }

    public Picture(){}
}
